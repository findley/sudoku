extern crate sudoku;
extern crate rand;
extern crate image;
extern crate imageproc;

#[cfg(test)]
mod tests {
    use std::path::Path;
    use rand::prelude::*;
    use sudoku::*;
    use sudoku::bit_matrix::BitMatrix;
    use sudoku::matrix::Matrix;
    use image;
    use imageproc;

    #[test]
    fn integration_test() {
        let file = "sudoku_test_03";
        let mut img = image::open(Path::new(&format!("tests/{}.png", file))).unwrap_or_else(|e| {
            panic!("Failed to open: {}", e);
        });
        let lum = img.to_luma();

        let w = lum.width();
        let h = lum.height();
        let mut board = board::Board::new();

        // 1. Binarize the image
        let bm = binerize(&lum);

        // 2. Find the biggest blob
        let big_blob = blob::find_biggest_blob(&bm);

        // 3. Hough transform
        let accum = lines::hough_transform(&big_blob);

        // 4. Identify and group lines
        let candidateLines = lines::lines_from_accum(&accum, 0.3);
        let lineClusters = lines::find_line_clusters(&candidateLines);
        let lines = lines::reduce_clusters(&accum, &lineClusters);
        let mut segments: Vec<(i32, i32, i32, i32)> = Vec::with_capacity(lines.len());
        for (x, y) in lines.iter() {
            println!("Houghspace coords: ({}, {})", *x, *y);
            let (rho, theta) = lines::hough_space_to_line(*x, *y, w, h);
            println!("Line: rho = {}, theta = {}", rho, theta);
            let seg = lines::line_to_segment2(rho, theta, w, h);
            println!("Segment: {:?}", seg);
            segments.push(seg);
        }

        // 5. Detect board
        // lines::filter_lines(w, h, &lines, &mut board);

        save_bit_matrix(&bm, &board, format!("tests/{}_01_bin.png", file).to_string());
        save_bit_matrix(&big_blob, &board, format!("tests/{}_02_big_blob.png", file).to_string());
        save_accum(&accum, &lineClusters, &lines, format!("tests/{}_03_accum.png", file).to_string());
        save_segments(&big_blob, &segments, format!("tests/{}_04_segments.png", file).to_string());
    }

    fn save_segments(bm: &BitMatrix, segments: &Vec<(i32, i32, i32, i32)>, name: String) {
        let mut rgb = write_bit_matrix_to_image(&bm);

        for (x1, y1, x2, y2) in segments.iter() {
            imageproc::drawing::draw_line_segment_mut(&mut rgb, (*x1 as f32, *y1 as f32), (*x2 as f32, *y2 as f32), image::Rgb([255, 0, 0]));
        }

        let _ = rgb.save(name).unwrap_or_else(|e| {
            panic!("Failed to save: {}", e)
        });
    }

    fn write_bit_matrix_to_image(bm: &BitMatrix) -> image::RgbImage {
        let img = image::DynamicImage::new_rgb8(bm.get_width(), bm.get_height());
        let mut rgb = img.to_rgb();
        for (x, y, p) in rgb.enumerate_pixels_mut() {
            if bm.get(x, y) {
                *p = image::Rgb([0, 0, 0]);
            } else {
                *p = image::Rgb([255, 255, 255]);
            }
        }

        rgb
    }

    fn save_bit_matrix(bm: &BitMatrix, board: &board::Board, name: String) {
        let mut rgb = write_bit_matrix_to_image(&bm);
        let _ = rgb.save(name).unwrap_or_else(|e| {
            panic!("Failed to save: {}", e)
        });
    }

    fn save_accum(m: &Matrix<u32>, groups: &Vec<Vec<(u32, u32)>>, lines: &Vec<(u32, u32)>, name: String) {
        let mut max = 0;
        for (x, y) in m.coordinates() {
            if m.get(x, y) > max { max = m.get(x, y) };
        }

        let img = image::DynamicImage::new_rgb8(m.get_width(), m.get_height());
        let mut rgb = img.to_rgb();

        for (x, y, p) in rgb.enumerate_pixels_mut() {
            let lum = (m.get(x, y) as f32 / max as f32 * 255.) as u8;
            *p = image::Rgb([lum, lum, lum]);
        }

        // let mut rng = thread_rng();
        // for group in groups.iter() {
        //     let c = px!(rng.gen_range(0, 255), rng.gen_range(0, 255), rng.gen_range(0, 255));
        //     for (x, y) in group.iter() {
        //         img.set_pixel(*x, *y, c);
        //     }
        // }

        for (x, y) in lines.iter() {
            rgb.put_pixel(*x, *y, image::Rgb([255, 255, 0]));
        }

        let _ = rgb.save(name).unwrap_or_else(|e| {
            panic!("Failed to save: {}", e)
        });
    }
}
