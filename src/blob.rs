use bit_matrix::BitMatrix;

pub fn find_biggest_blob(m: &BitMatrix) -> BitMatrix {
    let max_area = m.get_width() * m.get_height();
    let mut biggest_area = 0;
    let mut biggest = BitMatrix::new(m.get_width(), m.get_height());
    let mut current = BitMatrix::new(m.get_width(), m.get_height());
    let mut visited = BitMatrix::new(m.get_width(), m.get_height());

    let y = m.get_height() / 2;

    // Horizontal scan through 1/3rd of the matrix
    for x in 0..(m.get_width() / 3) {
        if !m.get(x, y) { continue; }
        if visited.get(x, y) { continue; }

        // clear current
        blob_detect(x, y, &m, &mut current, &mut visited);
        let (x1, y1, x2, y2) = matrix_bounding_box(&current);
        let area = (x2 - x1) * (y2 - y1);
        if area > biggest_area {
            biggest_area = area;
            let tmp = biggest;
            biggest = current;
            current = tmp;
            if biggest_area as f32 / max_area as f32 > 0.6 {
                break;
            }
        }
        current.clear();
    }

    return biggest
}

fn blob_detect(x: u32, y: u32, m: &BitMatrix, blob: &mut BitMatrix, visited: &mut BitMatrix) {
    let mut pending_visit_stack = Vec::new();
    pending_visit_stack.push((x, y));

    while pending_visit_stack.len() > 0 {
        let (x, y) = pending_visit_stack.pop().unwrap();
        if visited.get(x, y) { continue; }
        for (kx, ky) in m.kernel(x, y, 1) {
            if kx == x && ky == y { continue; }
            if !m.get(kx, ky) { continue; }
            if visited.get(kx, ky) { continue; }
            pending_visit_stack.push((kx, ky));
        }
        visited.set(x, y, true);
        blob.set(x, y, true);
    }
}

fn matrix_bounding_box(m: &BitMatrix) -> (u32, u32, u32, u32) {
    let mut x1 = m.get_width();
    let mut y1 = m.get_height();
    let mut x2 = 0;
    let mut y2 = 0;
    for (x, y) in m.coordinates() {
        if !m.get(x, y) { continue; }
        if x < x1 { x1 = x; }
        if x > x2 { x2 = x; }
        if y < y1 { y1 = y; }
        if y > y2 { y2 = y; }
    }

    (x1, y1, x2, y2)
}
