pub struct PairingIterator {
    len: usize,
    i: usize,
}

impl PairingIterator {
    pub fn new(len: usize) -> PairingIterator {
        PairingIterator {
            len,
            i: 0,
        }
    }
}

impl Iterator for PairingIterator {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<(usize, usize)> {
        if self.i >= self.len - 1 {
            None
        } else {
            let r = Some((self.i, self.i+1));
            self.i += 1;
            r
        }
    }
}
