extern crate fixedbitset;

use self::fixedbitset::FixedBitSet;
use matrix::MatrixIndex;

pub struct BitMatrix {
    w: u32,
    h: u32,
    data: FixedBitSet,
}

impl BitMatrix {
    pub fn new(w: u32, h: u32) -> BitMatrix {
        let s = (w * h) as usize;
        let mut data = FixedBitSet::with_capacity(s);
        data.grow(s);
        BitMatrix {
            w: w,
            h: h,
            data: data,
        }
    }

    pub fn get_width(&self) -> u32 {
        self.w
    }

    pub fn get_height(&self) -> u32 {
        self.h
    }

    #[inline]
    pub fn get(&self, x: u32, y: u32) -> bool {
        let index = (y * self.w + x) as usize;
        self.data[index]
    }

    #[inline]
    pub fn set(&mut self, x: u32, y: u32, val: bool) {
        let index = (y * self.w + x) as usize;
        self.data.set(index, val);
    }

    pub fn clear(&mut self) {
        self.data.clear();
    }

    pub fn coordinates(&self) -> MatrixIndex {
        MatrixIndex::new(self.w, self.h)
    }

    pub fn kernel(&self, x: u32, y: u32, r: u32) -> MatrixIndex {
        MatrixIndex::new_sub(self.w, self.h, r, x, y)
    }
}
