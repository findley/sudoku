pub struct Board {
    pub v_lines: [(f32, f32); 10],
    pub h_lines: [(f32, f32); 10],
    pub bounding_squares: [(u32, u32, u32, u32); 9],
    pub numbers: [[Option<u8>; 9]; 9],
}

impl Board {
    pub fn new() -> Board {
        Board {
            v_lines: [(0., 0.); 10],
            h_lines: [(0., 0.); 10],
            bounding_squares: [(0, 0, 0, 0); 9],
            numbers: [[None; 9]; 9],
        }
    }
}
