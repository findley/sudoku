pub struct Matrix<T: Copy> {
    w: u32,
    h: u32,
    data: Vec<T>,
}

impl <T: Copy> Matrix<T> {
    pub fn new(w: u32, h: u32, default: T) -> Matrix<T> {
        let s = (w * h) as usize;
        let mut data = Vec::with_capacity(s);
        data.resize(s, default);
        Matrix {
            w: w,
            h: h,
            data: data,
        }
    }

    pub fn get_width(&self) -> u32 {
        self.w
    }

    pub fn get_height(&self) -> u32 {
        self.h
    }

    #[inline]
    pub fn get(&self, x: u32, y: u32) -> T {
        let index = (y * self.w + x) as usize;
        self.data[index]
    }

    #[inline]
    pub fn set(&mut self, x: u32, y: u32, val: T) {
        let index = (y * self.w + x) as usize;
        self.data[index] = val;
    }

    pub fn coordinates(&self) -> MatrixIndex {
        MatrixIndex::new(self.w, self.h)
    }

    pub fn kernel(&self, x: u32, y: u32, r: u32) -> MatrixIndex {
        MatrixIndex::new_sub(self.w, self.h, r, x, y)
    }
}

pub struct MatrixIndex {
    max_w: u32,
    max_h: u32,
    w: u32,
    h: u32,
    x: u32,
    y: u32,
    x_offset: u32,
    y_offset: u32,
    r: u32,
}

impl MatrixIndex {
    pub(crate) fn new(w: u32, h: u32) -> MatrixIndex {
        MatrixIndex {
            max_w: w,
            max_h: h,
            w,
            h,
            x: 0,
            y: 0,
            x_offset: 0,
            y_offset: 0,
            r: 0,
        }
    }

    pub(crate) fn new_sub(w: u32, h: u32, r: u32, x_offset: u32, y_offset: u32) -> MatrixIndex {
        MatrixIndex {
            max_w: w,
            max_h: h,
            w: r * 2 + 1,
            h: r * 2 + 1,
            x: 0,
            y: 0,
            x_offset,
            y_offset,
            r,
        }
    }
}

impl Iterator for MatrixIndex {
    type Item = (u32, u32);

    fn next(&mut self) -> Option<(u32, u32)> {
        if self.x < self.w && self.y < self.h {
            let x = wrap_add(self.x_offset, self.max_w, self.x as i32 - self.r as i32);
            let y = wrap_add(self.y_offset, self.max_h, self.y as i32 - self.r as i32);
            let this = Some((x, y));
            self.x += 1;
            if self.x == self.w {
                self.x = 0;
                self.y += 1;
            }
            this
        } else {
            None
        }
    }
}

fn wrap_add(val: u32, max: u32, delta: i32) -> u32 {
    let max_i = max as i32;
    (((val as i32 + delta) % max_i + max_i) % max_i) as u32
}
