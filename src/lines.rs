use std::f32::consts::PI;
use bit_matrix::BitMatrix;
use matrix::Matrix;
use pairing_iterator::PairingIterator;
use board::Board;
use std::cmp::Ordering;

pub static AXIS_MAX: u32 = 360;
pub static HOUGH_PHASE_SHIFT: f32 = PI / 4.;

/// Create a hough-space representation of the provided `BitMatrix`.
/// Returns an accumulator `Matrix<u32>` where higher values in the matrix
/// correspond to lines in the image.
/// TODO: Explain how the accumulator is scaled
pub fn hough_transform(mat: &BitMatrix) -> Matrix<u32> {
    let w = mat.get_width();
    let h = mat.get_height();
    let max_p = ((w*w + h*h) as f32).sqrt();
    let mut accum: Matrix<u32> = Matrix::new(AXIS_MAX, AXIS_MAX, 0);

    for (x, y) in mat.coordinates() {
        if !mat.get(x, y) { continue; }

        //let y = h - y_inv;

        for theta in 0..AXIS_MAX {
            let rads = theta as f32 / AXIS_MAX as f32 * PI + HOUGH_PHASE_SHIFT;
            let p = calc_orthogonal_line_dist(x as f32, y as f32, rads);
            let scaled_p = scale_rho(p, max_p);

            let new_val = accum.get(theta, scaled_p) + 1;
            accum.set(theta, scaled_p, new_val);
        }
    }

    accum
}

pub fn hough_space_to_line(x: u32, y: u32, w: u32, h: u32) -> (f32, f32) {
    let max_p = ((w*w + h*h) as f32).sqrt();
    let half_p_axis = (AXIS_MAX as f32 / 2.).round();

    let rho = (y as f32 - half_p_axis) / half_p_axis * max_p;
    let theta = (x as f32 / AXIS_MAX as f32 * PI + HOUGH_PHASE_SHIFT);

    (rho, theta)
}

pub fn line_to_segment2(rho: f32, theta: f32, w: u32, h: u32) -> (i32, i32, i32, i32) {
    let mut p1_x = 0.0;
    let mut p1_y = 0.0;

    let mut p2_x = 0.0;
    let mut p2_y = 0.0;

    if theta == 0.0 || theta == PI {
        // The line is verticle
        p1_x = rho.abs();
        p1_y = 0.0;

        p2_x = rho.abs();
        p2_y = h as f32;
    } else if theta == PI / 2.0 {
        // The line is horizontal
        p1_x = 0.0;
        p1_y = rho.abs();

        p2_x = w as f32;
        p2_y = rho.abs();
    } else if theta > PI / 4.0 && theta < 3.0 / 2.0 * PI {
        p1_x = rho / theta.cos();
        p1_y = 0.0;

        p2_x = rho / theta.cos() - h as f32 * theta.sin() / theta.cos();
        p2_y = h as f32;
    } else {
        p1_x = 0.0;
        p1_y = rho.abs() / theta.sin();

        p2_x = w as f32;
        p2_y = rho.abs() / theta.sin() - p2_x * theta.cos() / theta.sin();
    }

    (p1_x.round() as i32, p1_y.round() as i32, p2_x.round() as i32, p2_y.round() as i32)
}

pub fn line_to_segment(rho: f32, theta: f32, w: u32, h: u32) -> (i32, i32, i32, i32) {
    let mut p1_x = 0.0;
    let mut p1_y = 0.0;

    let mut p2_x = 0.0;
    let mut p2_y = 0.0;

    let alpha = theta % (PI / 2.0);
    let beta = PI / 2.0 - alpha;

    if theta == 0.0 || theta == PI {
        // The line is verticle
        p1_x = rho.abs();
        p1_y = 0.0;

        p2_x = rho.abs();
        p2_y = h as f32;
    } else if theta == PI / 2.0 {
        // The line is horizontal
        p1_x = 0.0;
        p1_y = rho.abs();

        p2_x = w as f32;
        p2_y = rho.abs();
    } else if theta > 0.0 && theta < PI / 2.0 {
        p1_x = 0.0;
        p1_y = rho.abs() / theta.sin();

        p2_x = rho.abs() / theta.sin();
        p2_y = 0.0;
    } else if theta > PI / 2.0 && theta < PI {
        p1_x = -rho / alpha.sin();
        p1_y = 0.0;

        p2_x = w as f32;
        if rho < 0.0 {
            p2_y = (w as f32 - p1_x.abs()) * alpha.sin() / beta.sin();
        } else {
            p2_y = (w as f32 + p1_x.abs()) * alpha.sin() / beta.sin();
        }
    }

    (p1_x.round() as i32, p1_y.round() as i32, p2_x.round() as i32, p2_y.round() as i32)
}

pub fn lines_from_accum(m: &Matrix<u32>, threshold: f32) -> BitMatrix {
    let mut result = BitMatrix::new(m.get_width(), m.get_height());
    let max_accum = matrix_max(m);
    for (x, y) in m.coordinates() {
        if m.get(x, y) as f32 / max_accum as f32 >= threshold {
            // Surpress non-maximas
            let min = m.kernel(x, y, 1).map(|(kx, ky)| m.get(kx, ky)).min().unwrap();
            if min < m.get(x, y) / 6 {
                result.set(x, y, true);
            }
        }
    }
    result
}

fn scale_rho(rho: f32, max_rho: f32) -> u32 {
    let half_axis = (AXIS_MAX as f32 / 2.).round();
    ((rho / max_rho * half_axis) + half_axis).round() as u32
}

pub fn find_line_clusters(lines: &BitMatrix) -> Vec<Vec<(u32, u32)>> {
    let mut groups = Vec::new();
    let mut visited = BitMatrix::new(lines.get_width(), lines.get_height());
    let mut current_group: Vec<(u32, u32)> = Vec::new();
    let mut pending_visit_stack = Vec::new();

    for (theta, p) in lines.coordinates() {
        if !lines.get(theta, p) {
            continue;
        }

        if visited.get(theta, p) {
            continue;
        }

        pending_visit_stack.push((theta, p));

        while pending_visit_stack.len() > 0 {
            let (x, y) = pending_visit_stack.pop().unwrap();
            if visited.get(x, y) { continue; }
            for (kx, ky) in lines.kernel(x, y, 2) {
                if kx == x && ky == y { continue; }
                if !lines.get(kx, ky) { continue; }
                if visited.get(kx, ky) { continue; }
                pending_visit_stack.push((kx, ky));
            }
            visited.set(x, y, true);
            current_group.push((x, y));
        }
        groups.push(current_group);
        current_group = Vec::new();
    }

    groups
}

pub fn reduce_clusters(accum: &Matrix<u32>, clusters: &Vec<Vec<(u32, u32)>>) -> Vec<(u32, u32)> {
    let mut result = Vec::new();
    for cluster in clusters.iter() {
        result.push(*(cluster.iter().max_by_key(|(x, y)| accum.get(*x, *y)).unwrap()));
    }
    result
}

pub fn filter_lines(w: u32, h: u32, lines: &Vec<(u32, u32)>, board: &mut Board) {
    let vert_lines: Vec<(u32, u32)> = lines.iter().filter(|(x, _y)| *x < AXIS_MAX / 2).map(|p| *p).collect();
    let horiz_lines: Vec<(u32, u32)> = lines.iter().filter(|(x, _y)| *x >= AXIS_MAX / 2).map(|p| *p).collect();

    assert!(vert_lines.len() <= 10);
    assert!(horiz_lines.len() <= 10);

    let max_p = ((w*w + h*h) as f32).sqrt();
    let p_axis_max = (AXIS_MAX * w) / h;
    let half_p_axis = (p_axis_max as f32 / 2.).round();

    for (i, (shifted_theta, scaled_rho)) in vert_lines.iter().enumerate() {
        let rho = (*scaled_rho as f32 - half_p_axis) / half_p_axis * max_p;
        let theta = (*shifted_theta as f32) / 180. * PI + PI / 2.;
        board.v_lines[i] = (rho, theta)
    }

    for (i, (shifted_theta, scaled_rho)) in horiz_lines.iter().enumerate() {
        let rho = (*scaled_rho as f32 - half_p_axis) / half_p_axis * max_p;
        let theta = (*shifted_theta as f32) / 180. * PI + PI / 2.;
        board.h_lines[i] = (rho, theta)
    }

    //let (vx, vy) = mode_vector(&vert_lines);
    //let (hx, hy) = mode_vector(&horiz_lines);

    //println!("Vertical mode vector: ({}, {})", vx, vy);
    //println!("Horizontal mode vector: ({}, {})", hx, hy);
}

fn normal_to_yint(max_p: f32, half_p_axis: f32, h_theta: u32, h_rho: u32) -> (f32, f32) {
    let rho = (h_rho as f32 - half_p_axis) / half_p_axis * max_p;
    let theta = (h_theta as f32) / 180. * PI + PI / 2.;

    let m = -theta.cos() / theta.sin();
    let b = rho / theta.cos();

    (m, b)
}

fn matrix_max(m: &Matrix<u32>) -> u32 {
    m.coordinates().map(|(x, y)| m.get(x, y)).max().unwrap()
}

fn calc_orthogonal_line_dist(x: f32, y: f32, a: f32) -> f32 {
    (x * a.cos() + y * a.sin())
}

fn mode_vector(points: &Vec<(u32, u32)>) -> (f32, f32) {
    let mut vecs = Vec::with_capacity(points.len() - 1);

    for (i1, i2) in PairingIterator::new(points.len()) {
        let (x1, y1) = points[i1];
        let (x2, y2) = points[i2];

        vecs.push((x2 as f32 - x1 as f32, y2 as f32 - y1 as f32));
    }
    vecs.sort_unstable_by(|(ax, ay), (bx, by)| {
        let aa: f32 = (ay / ax).atan();
        let ba: f32 = (by / bx).atan();
        let mut r = aa.partial_cmp(&ba).unwrap_or(Ordering::Less);
        if r == Ordering::Equal {
            r = (ay.hypot(*ax)).partial_cmp(&by.hypot(*bx)).unwrap_or(Ordering::Less);
        }
        r
    });

    println!("vecs: {:?}", vecs);

    vecs[vecs.len() / 2]
}
