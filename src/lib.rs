extern crate image;

pub mod derp;
pub mod bit_matrix;
pub mod matrix;
pub mod lines;
pub mod pairing_iterator;
pub mod board;
pub mod blob;

use bit_matrix::BitMatrix;
use image::*;

const MIN_DYNAMIC_RANGE: u8 = 24;

pub fn binerize(lum: &GrayImage) -> BitMatrix {
    let w = lum.width();
    let h = lum.height();

    let mut m = BitMatrix::new(w, h);

    let block_size: u32 = if w > h {
        (w as f32 / 30.) as u32
    } else {
        (h as f32 / 30.) as u32
    };

    let mut y = 0;
    while y < m.get_height() {
        let block_h = if (m.get_height() - y) < block_size {
            m.get_height() - y
        } else {
            block_size
        };

        let mut x = 0;
        while x < m.get_width() {
            let block_w = if (m.get_width() - x) < block_size {
                m.get_width() - x
            } else {
                block_size
            };

            let threshold = compute_block_threshold(x, y, block_w, block_h, &lum);
            process_block(x, y, block_w, block_h, threshold, &lum, &mut m);
            x += block_size;
        }
        y += block_size;
    }

    m
}

pub fn compute_block_threshold(
    x: u32,
    y: u32,
    block_w: u32,
    block_h: u32,
    lum: &GrayImage
) -> u8 {
    let mut max: u8 = 0;
    let mut min: u8 = 255;
    let mut mean: f32 = 0.;
    let mut std_dev: f32 = 0.;
    let area = (block_w * block_h) as f32;

    for j in y..y+block_h {
        for i in x..x+block_w {
            let p = lum.get_pixel(i, j).data[0];
            if p > max { max = p; }
            if p < min { min = p; }
            mean += p as f32;
        }
    }

    mean /= area;

    for j in y..y+block_h {
        for i in x..x+block_w {
            let p = lum.get_pixel(i, j).data[0] as f32;
            std_dev += (p - mean) * (p - mean);
        }
    }

    std_dev = (std_dev / area).sqrt();

    const K: f32 = 0.05;
    const R: f32 = 128.;
    let sauvola = (mean * (1. + K * ((std_dev / R) - 1.))) as u8;

    if (max - min) > MIN_DYNAMIC_RANGE {
        sauvola
    } else {
        0
    }
}

pub fn process_block(
    x: u32,
    y: u32,
    block_w: u32,
    block_h: u32,
    threshold: u8,
    lum: &GrayImage,
    mat: &mut BitMatrix,
) {
    for j in y..y+block_h {
        for i in x..x+block_w {
            let p = lum.get_pixel(i, j).data[0];
            if p < threshold {
                mat.set(i, j, true);
            }
        }
    }
}
