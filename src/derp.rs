extern crate bmp;

use std::path::Path;

pub fn get_5() -> i32 {
    return 5;
}

pub fn get_size(img_file: &str) -> (u32, u32) {
    let img = bmp::open(Path::new(img_file)).unwrap_or_else(|e| {
        panic!("Failed to open: {}", e);
    });

    return (img.get_width(), img.get_height());
}
